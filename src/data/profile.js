export const profile = {
    name: "Aubertin",
    surname: "Emmanuel",
    license: true,
    car: true,
    phone: null,
    email: "aubertin.emmanuel@gmail.com",
    links: [
        [
            "Gitlab",
            "https://www.gitlab.com/aTHO_",
            "gitlab.com/aTHO_"
        ],
        [
            "GitHub",
            "https://www.github.com/Athomisos",
            "github.com/Athomisos"
        ],
        [
            "LinkedIn",
            "https://www.linkedin.com/in/emmanuel-aubertin/",
            "@aubertin-emmanuel"
        ]
    ]
}