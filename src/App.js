import './css/style.css'
import FakeWindow from './components/FakeWindow.js'
import IdentityList from './components/IdentityList';
import {useState, useEffect} from 'react'

const sh_cmd = "./getCV.sh --user 'Aubertin Emmanuel' --lang 'fr'";
const genRanHex = size => [...Array(size)].map(() => Math.floor(Math.random() * 16).toString(16)).join('');
const asciiName = ["\n   ______     __    __     __    __     ______     __   __     __  __     ______     __ \n",
                    "  /\\  ___\\   /\\ \"-./  \\   /\\ \"-./  \\   /\\  __ \\   /\\ \"-.\\ \\   /\\ \\/\\ \\   /\\  ___\\   /\\ \\\n",
                    "  \\ \\  __\\   \\ \\ \\-./\\ \\  \\ \\ \\-./\\ \\  \\ \\  __ \\  \\ \\ \\-.  \\  \\ \\ \\_\\ \\  \\ \\  __\\   \\ \\ \\____\n",
                    "   \\ \\_____\\  \\ \\_\\ \\ \\_\\  \\ \\_\\ \\ \\_\\  \\ \\_\\ \\_\\  \\ \\_\\\\\"\\_\\  \\ \\_____\\  \\ \\_____\\  \\ \\_____\\\n",
                    "    \\/_____/   \\/_/  \\/_/   \\/_/  \\/_/   \\/_/\\/_/   \\/_/ \\/_/   \\/_____/   \\/_____/   \\/_____/ \n",
                    "   ______     __  __     ______     ______     ______     ______   __     __   __              \n",
                    "  /\\  __ \\   /\\ \\/\\ \\   /\\  == \\   /\\  ___\\   /\\  == \\   /\\__  _\\ /\\ \\   /\\ \"-.\\ \\ \n",
                    "  \\ \\  __ \\  \\ \\ \\_\\ \\  \\ \\  __<   \\ \\  __\\   \\ \\  __<   \\/_/\\ \\/ \\ \\ \\  \\ \\ \\-.  \\ \n",
                    "   \\ \\_\\ \\_\\  \\ \\_____\\  \\ \\_____\\  \\ \\_____\\  \\ \\_\\ \\_\\    \\ \\_\\  \\ \\_\\  \\ \\_\\\\\"\\_\\\n",
                    "    \\/_/\\/_/   \\/_____/   \\/_____/   \\/_____/   \\/_/ /_/     \\/_/   \\/_/   \\/_/ \\/_/ \n"                                                                       
                ]


export function App() {
    const [cvCmd, setCvCmd] = useState('');
    const [podHex] = useState(genRanHex(12));
    const [ranDelay, setranDelay] = useState(Math.random() * 150 + 50);
    const [asciiState, setAsciiState] = useState('');
    const [promt, setPromt] = useState('');
    const [userCmd, setUserCmd] = useState('');

    const promptJsX = <span className='prompt'>root@podFront-{podHex} <b>[<span className="git-branche">main</span>]</b> </span>;
    

    const handleKeyDown = (e) => {
        switch (e.key){
            case "Backspace":
                setUserCmd(userCmd.slice(0, userCmd.length-1));
                break;
            case "Control":
            case "Shift":
            case "Meta":
                console.log('Meta '+ e.key +' ignore')
                break;
            default:
                setUserCmd(userCmd + e.key);
        }
        
    }
    document.addEventListener("keydown", handleKeyDown);
    let shouldAppear = asciiState.length === asciiName.length;
    
    useEffect(() => {
        
        const timeout = setTimeout(() => {
            setCvCmd(sh_cmd.slice(0, cvCmd.length + 1));
            setranDelay(Math.random() * 180 + 50);
            return () => clearTimeout(timeout);
        }, ranDelay)
        if(cvCmd.length === sh_cmd.length)
        {
            const timeout = setTimeout(() => {
                setAsciiState(asciiName.slice(0, asciiState.length + 1));
                setranDelay(Math.random() * 120 + 80);
                return () => clearTimeout(timeout);
            }, ranDelay)
            }

            { shouldAppear ? setPromt(<span>{promptJsX} <span>{userCmd}</span><span className="blink">█</span></span>) : setPromt("")}
              
    }, [cvCmd, asciiState])

    return (
        <div className="App">
            <FakeWindow
                title="root - Terminal"
                content={
                <div>
                    <code>
                        {promptJsX}<span>{cvCmd}</span>{ cvCmd.length !== sh_cmd.length ? <span className="blink">█</span> : '' }
                        { cvCmd.length === sh_cmd.length ? <div className='ascii-name-div'><span className='break-line ascii-name'>{asciiState}</span></div> : "" } 
                        { shouldAppear ? <IdentityList></IdentityList> : ''}
                        {promt}
                    </code>
                </div>}
            ></FakeWindow>
        </div>
    );
}
export default App;
