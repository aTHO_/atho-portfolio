
import { profile } from '../data/profile';
import { fr } from '../data/lang/lang.fr';


export default function IdentityList(){
    const profileToJSX = (key, value) => {
        switch(key){
            case "car":
            case "license":
                if(value){
                    return <li>{fr[key]} : [<span className='git-branche'>OK</span>]  </li>
                }
                break;
            case "links":
                let tempLinkState = [];
                value.map((link) => {
                    tempLinkState.push(<li>{link[0]} : <a href={link[1]} target="_blank" rel="noreferrer" >{link[2]}</a></li>);
                });
                return tempLinkState;
                break;
            default:
                if(value)
                {
                    return <li>{fr[key]} : {value}</li>
                }
                
        }
    }
    return (<ul className="info-list">{Object.keys(profile).map((key) => (profileToJSX(key, profile[key])))}</ul>);
}