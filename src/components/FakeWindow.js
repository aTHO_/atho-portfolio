export default function FakeWindow({title, content})
{
    return ( 
        <div className="fake-window">
            <div>
                <header>
                    <button className="red-button"></button>
                    <button className="yellow-button"></button>
                    <button className="green-button"></button>
                    <p className="title">{title}</p>
                </header>
            </div>
            {content}
        </div>
    );
    
}